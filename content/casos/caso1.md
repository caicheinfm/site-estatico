---
title: "O Cão dos Baskervilles"
date: 2024-10-06
draft: false
---

<br>

Entre os inúmeros casos que resolvi, poucos se comparam à complexidade e mistério do Cão dos Baskervilles. Este caso, envolto em lendas antigas e superstições, levou-me, juntamente com meu amigo e colaborador, Dr. Watson, até as traiçoeiras charnecas de Dartmoor. Lá, uma maldição de família e um cão fantasmagórico colocavam em risco a vida do herdeiro da propriedade Baskerville.

<br> 

Tudo começou com a morte súbita de Sir Charles Baskerville, encontrada em circunstâncias misteriosas perto da entrada de sua residência. Relatos locais falavam de um cão espectral gigantesco que havia perseguido os Baskervilles por gerações, ligado a uma antiga maldição. Acreditava-se que o animal causara a morte de Sir Charles, o que deixou toda a comunidade em estado de alerta.

<br>

Quando Sir Henry Baskerville, o último herdeiro da família, chegou da América para assumir sua herança, logo se tornou o próximo alvo da suposta maldição. Com medo pela sua segurança, o Dr. Mortimer, amigo da família, procurou minha ajuda.

<br>

Eu, sempre um homem da razão, não descartei completamente o poder da superstição na mente humana, mas sabia que a verdade deveria estar escondida entre os fatos e as lendas. Enviando Watson à propriedade Baskerville para proteger Sir Henry e investigar mais a fundo, permaneci em Londres, observando os acontecimentos à distância até o momento certo para agir. Watson, por sua vez, encontrou detalhes intrigantes: gritos misteriosos, pistas abandonadas e personagens enigmáticos que poderiam estar diretamente envolvidos.

<br>

Após uma série de deduções, descobri que o verdadeiro culpado por trás da lenda do cão não era um fantasma, mas sim um plano criminoso habilmente elaborado por Jack Stapleton, um parente distante dos Baskervilles, que pretendia assassinar Sir Henry para reivindicar a herança da família. O "cão espectral" era, na verdade, um cão real, treinado e coberto com uma substância luminosa para parecer sobrenatural, usado por Stapleton para espalhar o terror e eliminar os herdeiros legítimos.

<br>

Esse caso permanece como um dos meus maiores triunfos, não só por sua complexidade, mas por minha habilidade de separar a superstição dos fatos, mostrando que a razão e a lógica são as melhores armas contra o desconhecido.