---
title: "O Estudo em Vermelho"
date: 2024-10-06
draft: false
---

<br>

"O Estudo em Vermelho" foi o caso que marcou o início de minha parceria com o Dr. John Watson e desafiou a polícia de Londres com sua complexidade.

<br>

Tudo começou quando o corpo de um homem foi encontrado em uma casa abandonada em Londres. A cena era perturbadoramente calma, sem sinais visíveis de violência, mas algo chamava a atenção: a palavra "RACHE" (que significa "vingança" em alemão) estava escrita em sangue na parede. O curioso é que o cadáver não apresentava ferimentos que explicassem a presença de sangue. Foi esse enigma que levou a Scotland Yard a procurar meus serviços, uma vez que minha reputação como detetive já começava a se firmar.

<br>

Com minha habilidade de observação e dedução, percebi rapidamente detalhes que haviam passado despercebidos para os outros. Eu sabia que o assassino não havia agido por impulso, mas sim por uma motivação pessoal. A investigação revelou que a vítima, Enoch Drebber, estava envolvida em uma trama de vingança que remontava aos desertos do oeste americano, relacionada a uma tragédia amorosa.

<br>

Enquanto as pistas apontavam para uma conexão com o idioma alemão, rapidamente concluí que a palavra "RACHE" era uma pista falsa. O verdadeiro mistério estava no passado da vítima, especificamente em suas conexões com a cidade de Salt Lake, Utah, onde ele e seu cúmplice, Joseph Stangerson, cometeram crimes terríveis anos antes.

<br>

Após uma série de deduções, descobri que o assassino era Jefferson Hope, um homem que buscava vingança pela morte de sua noiva, Lucy Ferrier, cujos pais haviam sido assassinados por Drebber e Stangerson. Hope seguiu os dois até Londres para, finalmente, cumprir seu juramento de justiça. Ele engenhosamente deu às suas vítimas uma escolha: dois comprimidos, sendo um deles envenenado. Drebber escolheu o comprimido errado, e a vingança de Hope foi concluída.

<br>

Esse caso não só consolidou minha reputação, mas também revelou uma história trágica de amor, traição e vingança. "O Estudo em Vermelho" permanece como um exemplo de que a justiça pode ser complexa, e que o passado nunca está realmente enterrado.