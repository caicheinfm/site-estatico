---
title: "O Signo dos Quatro"
date: 2024-10-06
draft: false
---

<br>

"O Signo dos Quatro" foi outro caso fascinante que testou minha capacidade de resolver mistérios intricados. Envolvendo tesouros perdidos, traições e um pacto secreto, essa investigação me levou a desenterrar segredos escondidos por décadas.

<br>

A jovem Mary Morstan me procurou após receber misteriosas pérolas enviadas anonimamente ao longo dos anos, cada uma acompanhada por uma data enigmática. Ela também contou sobre o desaparecimento de seu pai, um oficial militar que sumiu sem deixar vestígios há dez anos. A situação se tornou ainda mais alarmante quando Mary recebeu uma carta prometendo a revelação de um grande segredo, caso ela aceitasse se encontrar com o remetente.

<br>

Ao acompanhá-la, juntamente com Watson, descobri que Thaddeus Sholto, filho de um ex-colega de seu pai, possuía informações vitais sobre o paradeiro dele e sobre um vasto tesouro conhecido como o Tesouro de Agra.

<br>

Conforme eu me aprofundava no caso, descobri uma trama ainda mais complexa, envolvendo quatro cúmplices que haviam feito um pacto na Índia para dividir o tesouro entre si. Mas, como era de se esperar, traições e mortes se seguiram, e o tesouro desapareceu. O caso culminou em uma perseguição intensa pelo Rio Tâmisa, enquanto eu utilizava minhas habilidades dedutivas e científicas para rastrear Jonathan Small, o último sobrevivente do grupo, que havia roubado o tesouro por vingança.

<br>

No fim, grande parte do tesouro foi perdida, mas Watson encontrou algo de valor inestimável: o amor de Mary Morstan, com quem se casaria. Esse caso fortaleceu minha reputação como mestre da dedução e mostrou como mesmo as maiores riquezas podem ser destruídas pela ganância.
